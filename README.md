# PING PONG APP #

App for tracking match results.

### Main features ###

* Track match and game results
* Display current time
* Display mentioned messages from HipChat rooms
* Based on MVP pattern

### Libraries in project ###

#### Development ####

* [Android Support Library](https://developer.android.com/topic/libraries/support-library/revisions.html)
* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [RxJava 2](https://github.com/ReactiveX/RxJava)
* [RxAndroid 2](https://github.com/ReactiveX/RxAndroid)
* [Retrolambda](https://github.com/evant/gradle-retrolambda)
* [Retrofit 2](https://github.com/square/retrofit)
* [Dagger 2](https://github.com/google/dagger)
* [Gson](https://github.com/google/gson)
* [OkHttpLoggingInterceptor](https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor)

#### Testing ####
* [Mockito](http://site.mockito.org/)
* [JUnit](https://github.com/junit-team/junit4)