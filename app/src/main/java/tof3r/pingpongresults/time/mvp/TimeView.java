package tof3r.pingpongresults.time.mvp;

/**
 * Created by dawid on 2/19/17.
 */

public interface TimeView {
    void displayTime(String time);
}
