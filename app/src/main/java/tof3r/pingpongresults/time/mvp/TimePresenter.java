package tof3r.pingpongresults.time.mvp;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.IoScheduler;

/**
 * Created by dgl.
 */

public class TimePresenter {

    private static final String TIME_PATTERN = "%02d:%02d:%02d";
    private Flowable<String> timeFlowable;
    private TimeView timeView;
    private Disposable timeDisposable;

    public TimePresenter() {
        timeFlowable = Flowable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(new IoScheduler())
                .map(count -> formattedTime());

    }

    private String formattedTime() {
        Calendar c = Calendar.getInstance(TimeZone.getDefault());
        return String.format(Locale.getDefault(), TIME_PATTERN,
                c.get(Calendar.HOUR_OF_DAY),
                c.get(Calendar.MINUTE),
                c.get(Calendar.SECOND));
    }

    public void attach(TimeView timeView) {
        this.timeView = timeView;
        displayTime();
    }

    private void displayTime() {
        timeDisposable = timeFlowable.subscribe(time -> timeView.displayTime(time));
    }

    public void detach() {
        if (timeDisposable != null && !timeDisposable.isDisposed()) {
            timeDisposable.dispose();
        }
        this.timeView = null;
    }
}
