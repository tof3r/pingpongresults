package tof3r.pingpongresults;

import android.app.Application;

import tof3r.pingpongresults.di.components.DaggerHipChatComponent;
import tof3r.pingpongresults.di.components.DaggerMatchComponent;
import tof3r.pingpongresults.di.components.HipChatComponent;
import tof3r.pingpongresults.di.components.MatchComponent;
import tof3r.pingpongresults.di.modules.AppModule;
import tof3r.pingpongresults.di.modules.HipChatModule;
import tof3r.pingpongresults.di.modules.MatchModule;
import tof3r.pingpongresults.di.modules.NetworkModule;
import tof3r.pingpongresults.di.modules.TimeModule;

/**
 * Created by dawid on 3/19/17.
 */

public class PingPongApp extends Application {

    private static final String baseUrl = "https://api.hipchat.com/v2/";
    private HipChatComponent hipChatComponent;
    private MatchComponent matchComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        hipChatComponent = DaggerHipChatComponent.builder()
                .hipChatModule(new HipChatModule())
                .networkModule(new NetworkModule(baseUrl))
                .build();

        matchComponent = DaggerMatchComponent.builder()
                .appModule(new AppModule(this))
                .matchModule(new MatchModule())
                .timeModule(new TimeModule())
                .build();
    }

    public HipChatComponent getHipChatComponent() {
        return hipChatComponent;
    }

    public MatchComponent getMatchComponent() {
        return matchComponent;
    }
}
