package tof3r.pingpongresults.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tof3r.pingpongresults.hipchat.HipChatMessageFormatter;
import tof3r.pingpongresults.hipchat.HipChatMessageObserver;
import tof3r.pingpongresults.hipchat.mvp.HipChatPresenter;
import tof3r.pingpongresults.network.api.HipChatApiClient;
import tof3r.pingpongresults.network.api.RetrofitApiService;

/**
 * Created by dawid on 3/19/17.
 */

@Module(includes = {NetworkModule.class})
public class HipChatModule {

    @Singleton
    @Provides
    HipChatPresenter provideHipChatPresenter(HipChatMessageObserver hipChatMessageObserver) {
        return new HipChatPresenter(hipChatMessageObserver);
    }

    @Singleton
    @Provides
    HipChatMessageObserver provideHipChatMessageObserver(HipChatMessageFormatter hipChatMessageFormatter) {
        return new HipChatMessageObserver(hipChatMessageFormatter);
    }

    @Singleton
    @Provides
    HipChatMessageFormatter provideHipChatMessageFormatter(HipChatApiClient hipChatApiClient) {
        return new HipChatMessageFormatter(hipChatApiClient);
    }

    @Singleton
    @Provides
    HipChatApiClient provideHipChatApiClient(RetrofitApiService retrofitApiService) {
        return new HipChatApiClient(retrofitApiService);
    }
}
