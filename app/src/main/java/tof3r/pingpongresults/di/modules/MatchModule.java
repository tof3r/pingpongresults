package tof3r.pingpongresults.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tof3r.pingpongresults.StringProvider;
import tof3r.pingpongresults.match.mvp.MatchPresenter;

/**
 * Created by dgl.
 */
@Module(includes = {AppModule.class})
public class MatchModule {

    @Singleton
    @Provides
    MatchPresenter providerMatchPresenter(StringProvider stringProvider){
        return new MatchPresenter(stringProvider);
    }
}
