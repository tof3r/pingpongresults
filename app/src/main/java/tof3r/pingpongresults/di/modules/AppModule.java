package tof3r.pingpongresults.di.modules;

/**
 * Created by dawid on 3/19/17.
 */

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tof3r.pingpongresults.StringProvider;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Singleton
    @Provides
    StringProvider provideStringProvider(Application application) {
        return new StringProvider(application);
    }
}
