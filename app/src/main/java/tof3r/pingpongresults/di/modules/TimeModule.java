package tof3r.pingpongresults.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tof3r.pingpongresults.time.mvp.TimePresenter;

/**
 * Created by dgl.
 */
@Module
public class TimeModule {

    @Singleton
    @Provides
    TimePresenter provideTimePresenter() {
        return new TimePresenter();
    }
}
