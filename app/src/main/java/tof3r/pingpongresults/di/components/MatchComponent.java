package tof3r.pingpongresults.di.components;

import javax.inject.Singleton;

import dagger.Component;
import tof3r.pingpongresults.di.modules.MatchModule;
import tof3r.pingpongresults.di.modules.TimeModule;
import tof3r.pingpongresults.match.mvp.MatchFragment;

/**
 * Created by dgl.
 */
@Singleton
@Component(modules = {MatchModule.class, TimeModule.class})
public interface MatchComponent {
    void inject(MatchFragment matchFragment);
}
