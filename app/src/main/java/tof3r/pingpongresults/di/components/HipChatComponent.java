package tof3r.pingpongresults.di.components;

import javax.inject.Singleton;

import dagger.Component;
import tof3r.pingpongresults.di.modules.HipChatModule;
import tof3r.pingpongresults.di.modules.NetworkModule;
import tof3r.pingpongresults.hipchat.mvp.HipChatFragment;

/**
 * Created by dawid on 3/19/17.
 */
@Singleton
@Component(modules = {HipChatModule.class, NetworkModule.class})
public interface HipChatComponent {
    void inject(HipChatFragment hipChatFragment);
}
