package tof3r.pingpongresults;

import android.content.Context;

/**
 * Created by dgl.
 */
public class StringProvider {

    private Context context;

    public StringProvider(Context context) {
        this.context = context;
    }

    public String getBeginnerString(int checkedId) {
        String beginner;
        switch (checkedId) {
            case R.id.leftRadioButton:
                beginner = leftBegins();
                break;
            case R.id.rightRadioButton:
                beginner = rightBegins();
                break;
            default:
                beginner = selectBeginner();
        }
        return beginner;
    }

    private String leftBegins() {
        return String.format(context.getString(R.string.beginner_side), "Left");
    }

    private String rightBegins() {
        return String.format(context.getString(R.string.beginner_side), "Right");
    }

    private String selectBeginner() {
        return context.getString(R.string.select_side);
    }
}
