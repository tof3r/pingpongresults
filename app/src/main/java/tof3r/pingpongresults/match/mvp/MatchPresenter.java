package tof3r.pingpongresults.match.mvp;

import tof3r.pingpongresults.StringProvider;
import tof3r.pingpongresults.match.Match;
import tof3r.pingpongresults.match.game.Game;

/**
 * Created by dawid on 1/23/17.
 */
public class MatchPresenter {

    private static final int ZERO = 0;
    private Game game;
    private Match match;
    private MatchView matchView;

    private StringProvider stringProvider;

    public MatchPresenter(StringProvider stringProvider) {
        this.match = new Match(ZERO, ZERO);
        this.stringProvider = stringProvider;
    }

    public void newGame() {
        this.game = new Game(ZERO, ZERO);
        this.match.newGameStarts(game);
    }

    public void attach(MatchView matchView) {
        this.matchView = matchView;
    }

    public void detach() {
        this.matchView = null;
    }

    public void increaseLeftTeamResult() {
        game.increaseLeftTeamResult();
        matchView.showLeftTeamResult(String.valueOf(game.getLeftResult()));
    }

    public void increaseRightTeamResult() {
        game.increaseRightTeamResult();
        matchView.showRightTeamResult(String.valueOf(game.getRightResult()));
    }

    public void decreaseLeftTeamResult() {
        game.decreaseLeftTeamResult();
        matchView.showLeftTeamResult(String.valueOf(game.getLeftResult()));
    }

    public void decreaseRightTeamResult() {
        game.decreaseRightTeamResult();
        matchView.showRightTeamResult(String.valueOf(game.getRightResult()));
    }

    public void switchSide() {
        game.switchServingSide();
        matchView.displayServingSide(game.getServing().getServingSide());
    }

    public void endGame() {
        game.endGame();
        match.storeGameResult();
        clearGameView();
    }

    private void clearGameView() {
        matchView.showLeftMatchResult(String.valueOf(match.getLeftMatchResult()));
        matchView.showRightMatchResult(String.valueOf(match.getRightMatchResult()));
        matchView.showLeftTeamResult(String.valueOf(ZERO));
        matchView.showRightTeamResult(String.valueOf(ZERO));
    }

    public void selectServing(int beginnerRadioId) {
        match.selectServing(beginnerRadioId);
        matchView.displayServingSide(game.getServing().getServingSide());
    }

    public void whoStarts(int checkedId) {
        String beginner = stringProvider.getBeginnerString(checkedId);
        matchView.displayWhoStarts(beginner);
    }
}
