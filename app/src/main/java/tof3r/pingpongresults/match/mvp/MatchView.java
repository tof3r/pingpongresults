package tof3r.pingpongresults.match.mvp;

import tof3r.pingpongresults.match.game.ServingSide;

/**
 * Created by dawid on 1/23/17.
 */
public interface MatchView {
    void showLeftTeamResult(String leftResult);

    void showRightTeamResult(String rightResult);

    void displayServingSide(ServingSide servingSide);

    void showLeftMatchResult(String leftMatchResult);

    void showRightMatchResult(String rightMatchResult);

    void displayWhoStarts(String beginner);
}
