package tof3r.pingpongresults.match.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tof3r.pingpongresults.PingPongApp;
import tof3r.pingpongresults.R;
import tof3r.pingpongresults.match.game.ServingSide;
import tof3r.pingpongresults.time.mvp.TimePresenter;
import tof3r.pingpongresults.time.mvp.TimeView;

/**
 * Created by dawid on 1/23/17.
 */

public class MatchFragment extends Fragment implements MatchView, TimeView {

    @BindView(R.id.leftTeamResult)
    TextView leftTeamResult;
    @BindView(R.id.rightTeamResult)
    TextView rightTeamResult;
    @BindView(R.id.totalResultLeft)
    TextView leftTotalResult;
    @BindView(R.id.totalResultRight)
    TextView rightTotalResult;
    @BindView(R.id.pingPongLeft)
    ImageView pingPongLeft;
    @BindView(R.id.pingPongRight)
    ImageView pingPongRight;
    @BindView(R.id.bottom_sheet)
    View bottomSheet;
    @BindView(R.id.beginnerRadioGroup)
    RadioGroup beginnerRadio;
    @BindView(R.id.whoStartsLabel)
    TextView whoStarts;
    @BindView(R.id.currentTime)
    TextView currentTime;

    @Inject
    MatchPresenter matchPresenter;

    @Inject
    TimePresenter timePresenter;

    private BottomSheetBehavior sheetBehavior;

    public static MatchFragment newInstance() {
        return new MatchFragment();
    }

    public MatchFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.match_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        beginnerRadio.setOnCheckedChangeListener((radioGroup, checkedId) -> {
            matchPresenter.selectServing(checkedId);
            matchPresenter.whoStarts(checkedId);
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        ((PingPongApp) getActivity().getApplication())
                .getMatchComponent()
                .inject(this);

        matchPresenter.attach(this);
        matchPresenter.newGame();

        timePresenter.attach(this);
    }

    @Override
    public void onStop() {
        matchPresenter.detach();
        timePresenter.detach();
        super.onStop();
    }

    @Override
    public void showLeftTeamResult(String leftResult) {
        leftTeamResult.setText(leftResult);
    }

    @Override
    public void showRightTeamResult(String rightResult) {
        rightTeamResult.setText(rightResult);
    }

    @Override
    public void showLeftMatchResult(String leftMatchResult) {
        leftTotalResult.setText(leftMatchResult);
    }

    @Override
    public void showRightMatchResult(String rightMatchResult) {
        rightTotalResult.setText(rightMatchResult);
    }

    @Override
    public void displayWhoStarts(String beginner) {
        whoStarts.setText(beginner);
    }

    @OnClick(R.id.leftTeam)
    public void increaseLeft() {
        leftResultPlus();
        switchServingSide();
    }

    @OnClick(R.id.leftTeamMinus)
    public void decreaseLeft() {
        leftResultMinus();
        switchServingSide();
    }

    @OnClick(R.id.rightTeam)
    public void increaseRight() {
        rightResultPlus();
        switchServingSide();
    }

    @OnClick(R.id.rightTeamMinus)
    public void decreaseRight() {
        rightResultMinus();
        switchServingSide();
    }

    @OnClick(R.id.endGame)
    public void gameEnds() {
        endGame();
    }

    public void leftResultPlus() {
        matchPresenter.increaseLeftTeamResult();
    }

    public void rightResultPlus() {
        matchPresenter.increaseRightTeamResult();
    }

    public void leftResultMinus() {
        matchPresenter.decreaseLeftTeamResult();
    }

    public void rightResultMinus() {
        matchPresenter.decreaseRightTeamResult();
    }

    public void endGame() {
        matchPresenter.endGame();
        prepareBottomSheet();
        prepareNewGame();
    }

    private void prepareBottomSheet() {
        beginnerRadio.clearCheck();
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void prepareNewGame() {
        matchPresenter.newGame();
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void switchServingSide() {
        matchPresenter.switchSide();
    }

    @Override
    public void displayServingSide(ServingSide servingSide) {
        switch (servingSide) {
            case LEFT:
                setLeftPingPongVisible();
                setRightPingPongInvisible();
                break;
            case RIGHT:
                setLeftPingPongInvisible();
                setRightPingPongVisible();
                break;
            default:
                setLeftPingPongInvisible();
                setRightPingPongInvisible();
        }
    }

    private void setLeftPingPongVisible() {
        pingPongLeft.setVisibility(View.VISIBLE);
    }

    private void setLeftPingPongInvisible() {
        pingPongLeft.setVisibility(View.INVISIBLE);
    }

    private void setRightPingPongVisible() {
        pingPongRight.setVisibility(View.VISIBLE);
    }

    private void setRightPingPongInvisible() {
        pingPongRight.setVisibility(View.INVISIBLE);
    }

    @Override
    public void displayTime(String time) {
        currentTime.setText(time);
    }
}
