package tof3r.pingpongresults.match.game;

/**
 * Created by dgl.
 */
public class Serving {
    private ServingSide servingSide;

    public Serving() {
        this.servingSide = ServingSide.NONE;
    }

    public ServingSide getServingSide() {
        return servingSide;
    }

    public void setServingSide(ServingSide servingSide) {
        this.servingSide = servingSide;
    }

    public void toggleBeginners() {
        if (servingSide != null && servingSide != ServingSide.NONE) {
            servingSide = servingSide == ServingSide.LEFT ? ServingSide.RIGHT : ServingSide.LEFT;
        }
    }
}
