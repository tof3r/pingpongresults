package tof3r.pingpongresults.match.game;

/**
 * Created by dgl.
 */

public enum ServingSide {
    NONE(-1),
    LEFT(0),
    RIGHT(1);

    private final int value;
    ServingSide(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
