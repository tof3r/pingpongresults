package tof3r.pingpongresults.match.game;

/**
 * Created by dgl.
 */

public class Game {

    private int leftResult;
    private int rightResult;
    private int matchLeftResult;
    private int matchRightResult;
    private Serving serving;

    public Game(int leftResult, int rightResult) {
        this.leftResult = leftResult;
        this.rightResult = rightResult;
        this.matchLeftResult = 0;
        this.matchRightResult = 0;
        this.serving = new Serving();
    }

    public int getLeftResult() {
        return leftResult;
    }

    public int getRightResult() {
        return rightResult;
    }

    public int getMatchLeftResult() {
        return matchLeftResult;
    }

    public int getMatchRightResult() {
        return matchRightResult;
    }

    public Serving getServing() {
        return serving;
    }

    public void decreaseRightTeamResult() {
        rightResult = preventResultToSetBelowZero(--rightResult);
    }

    public void decreaseLeftTeamResult() {
        leftResult = preventResultToSetBelowZero(--leftResult);
    }

    public void increaseRightTeamResult() {
        ++rightResult;
    }

    public void increaseLeftTeamResult() {
        ++leftResult;
    }

    public void increaseLeftMatchResult() {
        ++matchLeftResult;
    }

    public void increaseRightMatchResult() {
        ++matchRightResult;
    }

    public void switchServingSide() {
        if ((leftResult + rightResult) % 2 == 0) {
            serving.toggleBeginners();
        } else if (isDeuce()) {
            serving.toggleBeginners();
        }
        //TODO callback or return smmth
    }

    public void endGame() {
        checkWhoWins();
    }

    private void checkWhoWins() {
        if (leftResult > rightResult) {
            increaseLeftMatchResult();
        } else if (rightResult > leftResult) {
            increaseRightMatchResult();
        }
    }

    private boolean isDeuce() {
        return leftResult >= 10 && rightResult >= 10;
    }

    private int preventResultToSetBelowZero(int resultToDecrease) {
        return resultToDecrease <= 0 ? 0 : resultToDecrease;
    }

    @Override
    public String toString() {
        return "Game{" +
                "servingSide=" + serving.getServingSide() +
                ", leftResult=" + leftResult +
                ", rightResult=" + rightResult +
                '}';
    }
}
