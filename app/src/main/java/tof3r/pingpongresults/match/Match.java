package tof3r.pingpongresults.match;

import java.util.ArrayList;
import java.util.List;

import tof3r.pingpongresults.R;
import tof3r.pingpongresults.match.game.Game;
import tof3r.pingpongresults.match.game.ServingSide;

/**
 * Created by dgl.
 */

public class Match {
    private Game game;
    private List<Game> gameList;
    private int leftMatchResult;
    private int rightMatchResult;

    public Match(int leftMatchResult, int rightMatchResult) {
        this.gameList = new ArrayList<>();
        this.leftMatchResult = leftMatchResult;
        this.rightMatchResult = rightMatchResult;
    }

    public void newGameStarts(Game game) {
        this.gameList.add(game);
    }

    public int getLeftMatchResult() {
        return leftMatchResult;
    }

    public int getRightMatchResult() {
        return rightMatchResult;
    }

    public int getSizeOfGameList() {
        return gameList.size();
    }

    public void storeGameResult() {
        game = getCurrentGame();
        leftMatchResult += game.getMatchLeftResult();
        rightMatchResult += game.getMatchRightResult();
    }

    Game getCurrentGame() {
        int size = gameList.size() - 1;
        return gameList.get(size >= 0 ? size : 0);
    }

    public void selectServing(int beginnerRadioId) {
        game = getCurrentGame();
        switch (beginnerRadioId) {
            case R.id.leftRadioButton:
                game.getServing().setServingSide(ServingSide.LEFT);
                break;
            case R.id.rightRadioButton:
                game.getServing().setServingSide(ServingSide.RIGHT);
                break;
            default:
                game.getServing().setServingSide(ServingSide.NONE);
        }
    }

}
