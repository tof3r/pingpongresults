package tof3r.pingpongresults;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import tof3r.pingpongresults.hipchat.mvp.HipChatFragment;
import tof3r.pingpongresults.match.mvp.MatchFragment;

public class MainActivity extends AppCompatActivity {

    private MatchFragment matchFragment;
    private HipChatFragment hipChatFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        matchFragment = MatchFragment.newInstance();
        hipChatFragment = HipChatFragment.newInstance();
        putFragmentInContainer(R.id.matchContainer, matchFragment);
        putFragmentInContainer(R.id.hipChatContainer, hipChatFragment);

    }

    private void putFragmentInContainer(int containerResId, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerResId, fragment)
                .commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                matchFragment.leftResultPlus();
                matchFragment.switchServingSide();
                handled = true;
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                matchFragment.rightResultPlus();
                matchFragment.switchServingSide();
                handled = true;
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                matchFragment.leftResultMinus();
                matchFragment.switchServingSide();
                handled = true;
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                matchFragment.rightResultMinus();
                matchFragment.switchServingSide();
                handled = true;
                break;
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                matchFragment.endGame();
                handled = true;
                break;
        }
        return handled || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        hipChatFragment = null;
        matchFragment = null;
        super.onDestroy();
    }
}
