package tof3r.pingpongresults.hipchat;

import io.reactivex.disposables.CompositeDisposable;
import tof3r.pingpongresults.hipchat.mvp.HipChatView;

/**
 * Created by dgl.
 */

public class HipChatMessageObserver {
    private static final String ANDROID_ROOM_NAME = "North - Android Developers";
    private static final String GENERAL_ROOM_NAME = "North - Mobile - General";

    private HipChatMessageFormatter hipChatMessageFormatter;

    private HipChatView hipChatView;
    private CompositeDisposable compositeDisposable;

    public HipChatMessageObserver(HipChatMessageFormatter hipChatMessageFormatter) {
        this.hipChatMessageFormatter = hipChatMessageFormatter;
        this.compositeDisposable = new CompositeDisposable();
    }

    public void observeOnView(HipChatView hipChatView) {
        this.hipChatView = hipChatView;
    }

    public void startWatchingRooms() {
        watchAndroidRoom();
        watchGeneralRoom();
    }

    private void watchAndroidRoom() {
        hipChatMessageFormatter.observeOnView(hipChatView);
        compositeDisposable.add(hipChatMessageFormatter.getMessagesFromRoom(ANDROID_ROOM_NAME)
                .subscribe(items -> hipChatView.displayAndroidRoomMentions(items)));
    }

    private void watchGeneralRoom() {
        hipChatMessageFormatter.observeOnView(hipChatView);
        compositeDisposable.add(hipChatMessageFormatter.getMessagesFromRoom(GENERAL_ROOM_NAME)
                .subscribe(items -> hipChatView.displayGeneralRoomMentions(items)));
    }

    public void stopWatchingRooms() {
        hipChatMessageFormatter.stopObserving();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.clear();
        }
    }

    public void stopObserving() {
        this.hipChatView = null;
    }
}
