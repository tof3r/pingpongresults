package tof3r.pingpongresults.hipchat.mvp;

import tof3r.pingpongresults.hipchat.HipChatMessageObserver;

/**
 * Created by dgl.
 */

public class HipChatPresenter {

    private HipChatMessageObserver hipChatMessageObserver;

    public HipChatPresenter(HipChatMessageObserver hipChatMessageObserver) {
        this.hipChatMessageObserver = hipChatMessageObserver;
    }

    void attach(HipChatView hipChatView) {
        hipChatMessageObserver.observeOnView(hipChatView);
        hipChatMessageObserver.startWatchingRooms();
    }

    void detach() {
        hipChatMessageObserver.stopWatchingRooms();
        hipChatMessageObserver.stopObserving();
    }
}
