package tof3r.pingpongresults.hipchat.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import tof3r.pingpongresults.PingPongApp;
import tof3r.pingpongresults.R;
import tof3r.pingpongresults.hipchat.adapters.HipChatRoomAdapter;
import tof3r.pingpongresults.network.models.Item;

/**
 * Created by dgl.
 */

public class HipChatFragment extends Fragment implements HipChatView {

    @BindView(R.id.leftRoom)
    RecyclerView leftRoom;
    @BindView(R.id.rightRoom)
    RecyclerView rightRoom;
    @BindView(R.id.errorCardView)
    CardView errorCardView;
    @BindView(R.id.errorText)
    TextView errorText;

    @Inject
    protected HipChatPresenter hipChatPresenter;

    public static HipChatFragment newInstance() {
        return new HipChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hipchat_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager leftLM = getLinearLayoutManager(view.getContext());
        LinearLayoutManager rightLM = getLinearLayoutManager(view.getContext());
        leftRoom.setLayoutManager(leftLM);
        rightRoom.setLayoutManager(rightLM);
    }

    @NonNull
    private LinearLayoutManager getLinearLayoutManager(Context context) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        return linearLayoutManager;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((PingPongApp) getActivity().getApplication())
                .getHipChatComponent().inject(this);
        hipChatPresenter.attach(this);
    }

    @Override
    public void onStop() {
        hipChatPresenter.detach();
        super.onStop();
    }

    @Override
    public void displayAndroidRoomMentions(List<Item> items) {
        HipChatRoomAdapter adapter = new HipChatRoomAdapter(items);
        leftRoom.setAdapter(adapter);
    }

    @Override
    public void displayGeneralRoomMentions(List<Item> items) {
        HipChatRoomAdapter adapter = new HipChatRoomAdapter(items);
        rightRoom.setAdapter(adapter);
    }

    @Override
    public void showErrorView() {
        errorCardView.setVisibility(View.VISIBLE);
        errorText.setText(getString(R.string.no_internet_available));
    }

    @Override
    public void hideErrorView() {
        errorCardView.setVisibility(View.GONE);
    }
}
