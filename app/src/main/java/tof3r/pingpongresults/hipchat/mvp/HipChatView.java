package tof3r.pingpongresults.hipchat.mvp;

import java.util.List;

import tof3r.pingpongresults.network.models.Item;

/**
 * Created by dgl.
 */

public interface HipChatView {
    void displayAndroidRoomMentions(List<Item> items);

    void displayGeneralRoomMentions(List<Item> items);

    void showErrorView();

    void hideErrorView();
}
