package tof3r.pingpongresults.hipchat;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;
import tof3r.pingpongresults.hipchat.mvp.HipChatView;
import tof3r.pingpongresults.network.api.HipChatApiClient;
import tof3r.pingpongresults.network.models.Item;

/**
 * Created by dgl.
 */

public class HipChatMessageFormatter {

    private HipChatView hipChatView;

    private HipChatApiClient hipChatClient;

    public HipChatMessageFormatter(HipChatApiClient hipChatClient) {
        this.hipChatClient = hipChatClient;
    }

    void observeOnView(HipChatView hipChatView) {
        this.hipChatView = hipChatView;
    }

    @NonNull
    Flowable<List<Item>> getMessagesFromRoom(String roomName) {
        return this.hipChatClient.getRoomHistory(roomName)
                .flatMap(getOnlyMentionedMessages());
    }

    private void showErrorIfNeeded(@io.reactivex.annotations.NonNull List<Item> items) {
        if (items.size() == 0) {
            hipChatView.showErrorView();
        } else {
            hipChatView.hideErrorView();
        }
    }

    @NonNull
    private Function<List<Item>, Flowable<List<Item>>> getOnlyMentionedMessages() {
        return itemList -> {
            showErrorIfNeeded(itemList);
            return Flowable.fromIterable(itemList)
                    .filter(item -> !item.getMentions().isEmpty())
                    .toList().toFlowable();
        };
    }

    void stopObserving() {
        this.hipChatView = null;
    }
}
