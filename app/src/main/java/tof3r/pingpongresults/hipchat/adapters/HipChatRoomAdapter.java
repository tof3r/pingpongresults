package tof3r.pingpongresults.hipchat.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import tof3r.pingpongresults.R;
import tof3r.pingpongresults.network.models.Item;

/**
 * Created by dgl.
 */

/**
 * Created by dgl.
 */

public class HipChatRoomAdapter extends RecyclerView.Adapter<HipChatRoomAdapter.MessageViewHolder> {

    private List<Item> roomHistory;

    public HipChatRoomAdapter(List<Item> roomHistory) {
        this.roomHistory = roomHistory;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hip_chat_message, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return roomHistory.size();
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        final Item item = roomHistory.get(position);
        holder.from.setText(item.getFrom().getName());
        holder.message.setText(item.getMessage());
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.from)
        TextView from;
        @BindView(R.id.message)
        TextView message;

        public MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
