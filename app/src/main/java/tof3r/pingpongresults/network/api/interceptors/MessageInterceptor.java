package tof3r.pingpongresults.network.api.interceptors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by dgl.
 */

public class MessageInterceptor implements Interceptor {
    private static final String ITEMS = "items";
    private static final String TYPE = "type";
    private static final String MESSAGE = "message";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        String responseString = response.body().string();
        String filteredResponse = filterOnlyMessages(responseString);
        MediaType contentType = response.body().contentType();
        ResponseBody responseBody = ResponseBody.create(contentType, filteredResponse);
        return response.newBuilder().body(responseBody).build();
    }

    private String filterOnlyMessages(String responseString) {
        JSONArray filtered = new JSONArray();
        JSONObject newResponse = new JSONObject();
        try {
            parseResponse(responseString, filtered);
            newResponse.put(ITEMS, filtered);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newResponse.toString();
    }

    private void parseResponse(String responseString, JSONArray filtered) throws JSONException {
        JSONObject itemsObj = new JSONObject(responseString);
        JSONArray messagesArray = itemsObj.getJSONArray(ITEMS);
        for (int i = 0; i < messagesArray.length(); i++) {
            addOnlyMessages(filtered, messagesArray, i);
        }
    }

    private void addOnlyMessages(JSONArray filtered, JSONArray messagesArray, int i) throws JSONException {
        JSONObject message = messagesArray.getJSONObject(i);
        if (message.getString(TYPE).equals(MESSAGE)) {
            filtered.put(message);
        }
    }
}
