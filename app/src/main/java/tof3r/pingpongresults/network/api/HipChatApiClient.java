package tof3r.pingpongresults.network.api;

import android.support.annotation.NonNull;

import org.reactivestreams.Publisher;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.internal.schedulers.IoScheduler;
import tof3r.pingpongresults.network.models.Item;
import tof3r.pingpongresults.network.models.Items;

/**
 * Created by dgl.
 */

public class HipChatApiClient implements HipChatApi {
    private static final String MESSAGE_TOKEN = "RXHTO9qFtQNrCIKc6GvRv6rxtcthSyh1DYvaNhMu";

    private static final int MAX_RESULTS = 50;
    private static final int PERIOD = 30;
    private static final int INITIAL_DELAY = 0;

    private RetrofitApiService retrofitApiService;

    public HipChatApiClient(RetrofitApiService retrofitApiService) {
        this.retrofitApiService = retrofitApiService;
    }

    @Override
    public Flowable<List<Item>> getRoomHistory(String roomName) {
        return Flowable.interval(INITIAL_DELAY, PERIOD, TimeUnit.SECONDS, new IoScheduler())
                .flatMap(callForHistory(roomName))
                .flatMap(getMessagesHistory())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    private Function<Items, Publisher<List<Item>>> getMessagesHistory() {
        return items -> Flowable.just(items.getItems());
    }

    @NonNull
    private Function<Long, Publisher<Items>> callForHistory(final String roomName) {
        return __ -> retrofitApiService.getRoomHistory(roomName, MESSAGE_TOKEN, MAX_RESULTS)
                .onErrorReturn(throwable -> new Items());
    }
}
