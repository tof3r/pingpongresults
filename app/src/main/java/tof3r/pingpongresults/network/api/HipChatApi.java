package tof3r.pingpongresults.network.api;

import java.util.List;

import io.reactivex.Flowable;
import tof3r.pingpongresults.network.models.Item;

/**
 * Created by dgl.
 */

public interface HipChatApi {
    Flowable<List<Item>> getRoomHistory(String roomName);
}
