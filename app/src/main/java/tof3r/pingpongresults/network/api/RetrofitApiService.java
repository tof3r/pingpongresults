package tof3r.pingpongresults.network.api;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tof3r.pingpongresults.network.models.Items;

/**
 * Created by dgl.
 */

public interface RetrofitApiService {
    @GET("room/{roomName}/history")
    Flowable<Items> getRoomHistory(@Path("roomName") String roomName,
                                   @Query("auth_token") String auth_token,
                                   @Query("max-results") int results);
}
