package tof3r.pingpongresults.network.models;

import com.google.gson.annotations.Expose;

/**
 * Created by dgl.
 */

public class From {
    @Expose
    private Integer id;
    @Expose
    private String mentionName;
    @Expose
    private String name;
    @Expose
    private String version;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The mentionName
     */
    public String getMentionName() {
        return mentionName;
    }

    /**
     * @param mentionName The mention_name
     */
    public void setMentionName(String mentionName) {
        this.mentionName = mentionName;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version The version
     */
    public void setVersion(String version) {
        this.version = version;
    }
}
