package tof3r.pingpongresults.network.models;

/**
 * Created by dgl.
 */
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Item {

    @Expose
    private String date;
    @Expose
    private From from;
    @Expose
    private String id;
    @Expose
    private List<Mention> mentions = null;
    @Expose
    private String message;
    @Expose
    private String type;

    public Item() {
        this.date = "";
        this.from = null;
        this.id = "";
        this.mentions = new ArrayList<>();
        this.message = "";
        this.type = "";
    }

    public Item(String date, From from, String id, List<Mention> mentions, String message, String type) {
        this.date = date;
        this.from = from;
        this.id = id;
        this.mentions = mentions;
        this.message = message;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Mention> getMentions() {
        return mentions;
    }

    public void setMentions(List<Mention> mentions) {
        this.mentions = mentions;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
