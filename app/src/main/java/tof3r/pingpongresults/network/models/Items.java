package tof3r.pingpongresults.network.models;

/**
 * Created by dgl.
 */
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Items {

    @Expose
    private List<Item> items = new ArrayList<Item>();

    /**
     * @return The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

}
