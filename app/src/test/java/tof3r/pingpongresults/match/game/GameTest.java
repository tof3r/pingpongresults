package tof3r.pingpongresults.match.game;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dgl.
 */
public class GameTest {

    private static final int ZERO_POINT = 0;
    private static final int ONE_POINT = 1;

    private Game game;

    @Test
    public void shouldIncreaseLeftResult() throws Exception {
        game = new Game(0, 0);
        game.increaseLeftTeamResult();
        assertEquals(ONE_POINT, game.getLeftResult());
    }

    @Test
    public void shouldIncreaseRightResult() throws Exception {
        game = new Game(0, 0);
        game.increaseRightTeamResult();
        assertEquals(ONE_POINT, game.getRightResult());
    }

    @Test
    public void shouldDecreaseLeftResult_leftResultIsOne() throws Exception {
        game = new Game(2, 0);
        game.decreaseLeftTeamResult();
        assertEquals(ONE_POINT, game.getLeftResult());
    }

    @Test
    public void shouldDecreaseRightResult_rightResultIsOne() throws Exception {
        game = new Game(0, 2);
        game.decreaseRightTeamResult();
        assertEquals(ONE_POINT, game.getRightResult());
    }

    @Test
    public void shouldDecreaseLeftResultBelowZero_leftResultIsZero() throws Exception {
        game = new Game(0,0);
        game.decreaseLeftTeamResult();
        assertEquals(ZERO_POINT, game.getLeftResult());
    }

    @Test
    public void shouldDecreaseRightResultBelowZero_rightResultIsZero() throws Exception {
        game = new Game(0,0);
        game.decreaseRightTeamResult();
        assertEquals(ZERO_POINT, game.getRightResult());
    }

    @Test
    public void shouldNotChangingSide() throws Exception {
        game = new Game(1, 0);
        game.getServing().setServingSide(ServingSide.LEFT);

        game.switchServingSide();

        assertEquals(ServingSide.LEFT, game.getServing().getServingSide());
    }

    @Test
    public void shouldChangeSide() throws Exception {
        game = new Game(2, 0);
        game.getServing().setServingSide(ServingSide.LEFT);

        game.switchServingSide();

        assertEquals(ServingSide.RIGHT, game.getServing().getServingSide());
    }

    @Test
    public void shouldChangeSideWhenIsDeuce() throws Exception {
        game = new Game(10, 10);
        game.getServing().setServingSide(ServingSide.LEFT);

        game.switchServingSide();

        assertEquals(ServingSide.RIGHT, game.getServing().getServingSide());
    }

    @Test
    public void shouldEndGameWithWinningLeft() throws Exception {
        game = new Game(12, 10);
        game.endGame();
        assertEquals("Left match result should be one!", ONE_POINT, game.getMatchLeftResult());
    }

    @Test
    public void shouldEndGameWithWinningRight() throws Exception {
        game = new Game(10, 12);
        game.endGame();
        assertEquals("Right match result should be one!", ONE_POINT, game.getMatchRightResult());
    }

    @Test
    public void shouldEndGameWithNoWinner() throws Exception {
        game = new Game(12, 12);
        game.endGame();
        assertEquals("Left match result should be zero!", ZERO_POINT, game.getMatchLeftResult());
        assertEquals("Right match result should be zero!", ZERO_POINT, game.getMatchRightResult());
    }
}