package tof3r.pingpongresults.match.game;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dgl.
 */
public class ServingTest {

    private Serving serving;

    @Before
    public void setUp() throws Exception {
        serving = new Serving();
    }

    @Test
    public void shouldSetLeftAsServingSide() throws Exception {
        serving.setServingSide(ServingSide.LEFT);
        assertEquals(ServingSide.LEFT, serving.getServingSide());
    }

    @Test
    public void shouldSetRightAsServingSide() throws Exception {
        serving.setServingSide(ServingSide.RIGHT);
        assertEquals(ServingSide.RIGHT, serving.getServingSide());
    }

    @Test
    public void shouldSideChanged() throws Exception {
        serving.setServingSide(ServingSide.LEFT);
        serving.toggleBeginners();
        assertEquals(ServingSide.RIGHT, serving.getServingSide());
    }

    @Test
    public void shouldStayServingSideUnset() throws Exception {
        serving.toggleBeginners();
        assertEquals(ServingSide.NONE, serving.getServingSide());
    }

    @After
    public void tearDown() throws Exception {
        serving = null;
    }
}