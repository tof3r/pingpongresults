package tof3r.pingpongresults.match;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import tof3r.pingpongresults.R;
import tof3r.pingpongresults.match.game.Game;
import tof3r.pingpongresults.match.game.ServingSide;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by dgl.
 */
@RunWith(MockitoJUnitRunner.class)
public class MatchTest {
    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int NOBODY_SERVING = 0;
    private static final int LEFT_RADIO_BUTTON_ID = R.id.leftRadioButton;
    private static final int RIGHT_RADIO_BUTTON_ID = R.id.rightRadioButton;

    private Match match;

    @Before
    public void setUp() throws Exception {
        match = new Match(ZERO, ZERO);
    }

    @Test
    public void shouldLeftTeamStart() throws Exception {
        match.newGameStarts(new Game(ZERO, ZERO));

        match.selectServing(LEFT_RADIO_BUTTON_ID);

        assertEquals("Left team should start!", ServingSide.LEFT,
                match.getCurrentGame().getServing().getServingSide());
    }

    @Test
    public void shouldRightTeamStart() throws Exception {
        match.newGameStarts(new Game(ZERO, ZERO));

        match.selectServing(RIGHT_RADIO_BUTTON_ID);

        assertEquals("Right team should start!", ServingSide.RIGHT,
                match.getCurrentGame().getServing().getServingSide());
    }

    @Test
    public void shouldNoneStart() throws Exception {
        match.newGameStarts(new Game(ZERO, ZERO));

        match.selectServing(NOBODY_SERVING);

        assertEquals("Serving side should be unset!", ServingSide.NONE,
                match.getCurrentGame().getServing().getServingSide());
    }

    @Test
    public void shouldLeftTeamHaveOneMatchPoint() throws Exception {
        Game game = Mockito.mock(Game.class);
        when(game.getMatchLeftResult()).thenReturn(ONE);
        when(game.getMatchRightResult()).thenReturn(ZERO);
        match.newGameStarts(game);

        match.storeGameResult();

        assertEquals("Left team should have one point!", ONE, match.getLeftMatchResult());
    }

    @Test
    public void shouldRightTeamHaveOneMatchPoint() throws Exception {
        Game game = Mockito.mock(Game.class);
        when(game.getMatchLeftResult()).thenReturn(ZERO);
        when(game.getMatchRightResult()).thenReturn(ONE);
        match.newGameStarts(game);

        match.storeGameResult();

        assertEquals("Right team should have one point!", ONE, match.getRightMatchResult());
    }

    @Test
    public void shouldBeOneGameInList() throws Exception {
        match.newGameStarts(new Game(ZERO, ZERO));
        assertEquals("Game list should have one game!", ONE, match.getSizeOfGameList());
    }

    @Test
    public void shouldBeZeroGameInList() throws Exception {
        assertEquals("Game list should be empty!", ZERO, match.getSizeOfGameList());
    }

    @Test
    public void shouldGetCurrentGame() throws Exception {
        Game newGame = new Game(10, 8);
        match.newGameStarts(newGame);

        Game currentGame = match.getCurrentGame();

        assertEquals(newGame, currentGame);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldNotGetAnyGame() throws Exception {
        match.getCurrentGame();
    }
}