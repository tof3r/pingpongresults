package tof3r.pingpongresults.hipchat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import tof3r.pingpongresults.hipchat.mvp.HipChatView;
import tof3r.pingpongresults.network.api.HipChatApiClient;
import tof3r.pingpongresults.network.models.From;
import tof3r.pingpongresults.network.models.Item;
import tof3r.pingpongresults.network.models.Mention;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by dgl.
 */
@RunWith(MockitoJUnitRunner.class)
public class HipChatMessageFormatterTest {

    @Mock
    private HipChatView hipChatView;

    @Mock
    private HipChatApiClient hipChatApiClient;

    private HipChatMessageFormatter hipChatMessageFormatter;

    @Before
    public void setUp() throws Exception {
        hipChatMessageFormatter = new HipChatMessageFormatter(hipChatApiClient);
    }

    @Test
    public void shouldGetMessagesFromRoom() throws Exception {
        TestSubscriber<List<Item>> testSubscriber = new TestSubscriber<>();
        when(hipChatApiClient.getRoomHistory(anyString()))
                .thenReturn(Flowable.just(prepareFakeListItemWithMentions()));
        hipChatMessageFormatter.observeOnView(hipChatView);

        hipChatMessageFormatter.getMessagesFromRoom(anyString())
                .subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(items -> items.size() > 0);
        testSubscriber.assertComplete();
    }

    @Test
    public void shouldGetNoMessagesFromRoom() throws Exception {
        TestSubscriber<List<Item>> testSubscriber = new TestSubscriber<>();
        when(hipChatApiClient.getRoomHistory(anyString()))
                .thenReturn(Flowable.just(prepareFakeListItemWithoutMentions()));
        hipChatMessageFormatter.observeOnView(hipChatView);

        hipChatMessageFormatter.getMessagesFromRoom(anyString())
                .subscribe(testSubscriber);

        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(items -> items.size() == 0);
        testSubscriber.assertComplete();
    }

    private ArrayList<Item> prepareFakeListItemWithMentions() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new Item());

        ArrayList<Mention> mentions = new ArrayList<>();
        mentions.add(new Mention());
        items.add(new Item(" ", new From(), " ", mentions, " ", ""));
        return items;
    }

    private ArrayList<Item> prepareFakeListItemWithoutMentions() {
        ArrayList<Item> items = new ArrayList<>();
        items.add(new Item());
        items.add(new Item());
        return items;
    }
}